<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="assets/css/colorbox.css"/>
        <link rel="stylesheet" href="assets/css/default.css?ver=4"/>
        <link rel="icon" type="image/png" href="favicon.png"/>
        <title>Rewitalizacja Gminy Wisznice</title>
    </head>
    <body>
        <div class="container">
            <header id="header" class="clearfix">
                <div class="brand pull-left clearfix">
                    <a href="">
                        <img src="assets/images/logo.png" class="logo" alt="Radzy podlaski" />
                        <h1><span>Gmina</span><br />
                        Wisznice</h1>
                    </a>
                </div>
                <div class="upper-nav pull-right">
                    <ul class="unstyled inline">
                        <li>
                            <a href="#aktualnosci">Aktualności</a>
                        </li>
                        <li>
                            <a href="#cele">Cele rewitalizacji</a>
                        </li>
                        <li>
                            <a href="#kontakt">Kontakt</a>
                        </li>
                    </ul>
                </div>
            </header>
            <div id="content" class="clearfix">
                <div class="grey-bg"></div>
                <div class="aktualnosci clearfix" id="aktualnosci">
                    <div class="narrow-col section-title">
                        <h2>Aktualności</h2>
                        <img src="assets/images/aktualnosci.jpg" alt="Aktualności" />
                        <!--Możliwe że będą pliki do pobrania-->
                    </div>
                    <div class="wide-col clearfix">
                        <div class="item">
                            <div class="half-col">
                                <h3>Gmina Wisznice przystąpiła do opracowa-<br/>nia Lokalnego Programu Rewitalizacji<br/>Gminy Wisznice na lata 2017-2023</h3>
                                <p>Lokalny Program Rewitalizacji Gminy Wisznice będzie opracowany zgodnie z Wytycznymi
                                w zakresie rewitalizacji w programach operacyjnych na lata 2014–2020 zatwierdzonymi w dniu
                                3 lipca 2015 r. przez Ministra Infrastruktury i Rozwoju i realizowany w ramach projektu
                                pn. &quot;Przygotowanie Gminnego Programu Rewitalizacji obszaru kryzysowego na terenie Gminy
                                Wisznice&quot;, współfinansowanego ze środków Unii Europejskiej w ramach Programu
                                Operacyjnego Pomoc Techniczna 2014-2020.</p>
                                <p>Program ten będzie stanowił podstawę do podjęcia kompleksowych działań rewitalizacyjnych
                                na zdegradowanym obszarze Gminy Wisznice wymagających szczególnego wsparcia. Ponadto
                                umożliwi efektywne pozyskiwanie dofinansowania do projektów ze środków Unii Europejskiej
                                w perspektywie finansowej 2014–2020. Zaplanowane do realizacji projekty w ramach
                                Lokalnego Programu Rewitalizacji przyczynią się do pobudzenia aktywności społecznej i
                                przedsiębiorczości mieszkańców, przywrócenia estetyki i ładu przestrzennego, ochrony
                                środowiska naturalnego, zachowania dziedzictwa kulturowego, a tym samym poprawy jakości
                                życia społeczności lokalnej.</p>
                                <p>Rewitalizacja obszarów zdegradowanych będzie kompleksowym i efektywnym procesem,
                                mającym za zadanie wyprowadzenie obszarów zdegradowanych ze stanu kryzysowego,
                                spowodowanego koncentracją negatywnych zjawisk społecznych, gospodarczych,
                                środowiskowych, przestrzenno-funkcjonalnych i technicznych, przy aktywnym udziale i
                                współpracy z lokalną społecznością.</p>
                                <p>Pierwszym etapem prac nad opracowaniem Lokalnego Programu Rewitalizacji będzie
                                całościowa diagnoza Gminy Wisznice umożliwiająca wyznaczenie obszaru zdegradowanego
                                oraz obszaru rewitalizacji, charakteryzujących się największą kumulacją problemów społeczno-
                                gospodarczych i możliwych do wyzwolenia lokalnych potencjałów w ww. sferach.</p>
                                <p><strong>Zachęcamy wszystkich mieszkańców, organizacje pozarządowe, przedsiębiorców
                                i wszystkie inne osoby oraz podmioty, które chcą mieć wpływ na rozwój Gminy do
                                włączenia się do prac nad diagnozą i wyznaczeniem obszarów zdegradowanych i do
                                rewitalizacji.</strong></p>
                                <h4>Czym jest rewitalizacja?</h4>
                                <p>Rewitalizacja to zestaw działań mających na celu poprawę sytuacji w mieście, w obszarach, na
                                których zidentyfikowano stan kryzysowy (obszary problemowe). Działania te obejmują sferę
                                społeczną, gospodarczą, środowiskową, techniczną oraz przestrzenną. Dzięki ich realizacji
                                ma nastąpić poprawa warunków do życia, wypoczynku i rozwoju gospodarczego.</p>
                            </div>
                            <div class="half-col">
                                <h4>Czym jest Program Rewitalizacji?</h4>
                                <p>Program Rewitalizacji jest planem wyprowadzenia obszarów zdegradowanych ze stanu
                                kryzysowego. Działania zawarte w tym Programie będą służyły rozwiązaniu najistotniejszych
                                problemów społecznych danego obszaru oraz towarzyszących im problemów w sferze
                                gospodarczej, przestrzenno-funkcjonalnej, technicznej oraz środowiskowej. Prowadzone
                                działania będą miały charakter systemowy i posłużą rozwojowi społecznemu, gospodarczemu
                                oraz przestrzennemu. Będą mogły być finansowane ze środków publicznych (budżetu gminy,
                                kraju, środków europejskich) oraz prywatnych.</p>
                                <p>Udział społeczności lokalnej jest kluczowy dla opracowania Programu Rewitalizacji oraz
                                diagnozy społeczno-gospodarczej, w tym wyznaczenia obszarów rewitalizacji. Działania
                                planowane w ramach Programu mają służyć poprawie warunków życia i zwiększaniu
                                aktywności gospodarczej mieszkańców. Dlatego też zachęcamy do włączenia się do prac nad
                                dokumentem.</p>
                                <p>W związku z tym, zwracamy się do Państwa z prośbą o aktywne wzięcie udziału w procesie
                                    diagnostycznym gminy i wypełnienie poniżej zamieszczonej ankiety.</p>
                                <a href="assets/download/Ankieta_Wisznice.docx" class="file">Ankieta_Wisznice.docx</a>
                                <p>Ankietę będzie można wypełnić do dnia 07.04.2017 r.</p>
                                <p>Jednocześnie zapraszamy przedstawicieli sektorów publicznego, społecznego i gospodarczego
                                na spotkania konsultacyjne, które umożliwią poznanie oczekiwań i potrzeb interesariuszy
                                rewitalizacji oraz dokładną diagnozę Gminy Wisznice w celu wyznaczenia obszaru
                                zdegradowanego i obszaru rewitalizacji. Szczegółowe informacje dotyczące ww. spotkań
                                konsultacyjnych zostaną podane w późniejszym terminie.</p>
                                <p>Jesteśmy przekonani, że jako aktywni mieszkańcy, przedsiębiorcy i przedstawiciele organizacji
                                pozarządowych oraz instytucji publicznych, wniesiecie Państwo wiele uwag i propozycji, które
                                przyczynią się do zdiagnozowania kluczowych problemów gminy koniecznych do rozwiązania
                                w procesie rewitalizacji, a także lokalnych potencjałów.</p>
                                <p class="author"><strong>Wójt Gminy Wisznice<br/>Piotr Dragan</strong></p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="half-col">
                                <h3>Zaproszenie na konsultacje społeczne</h3>
                                <p>Gmina Wisznice realizuje projekt pn. Przygotowanie Gminnego Programu Rewitalizacji
                                obszaru kryzysowego na terenie Gminy Wisznice, który uzyskał dofinansowanie w
                                ramach konkursu dotacyjnego na „Przygotowywanie programów rewitalizacji”
                                ogłoszonego przez Urząd Marszałkowski Województwa Lubelskiego - III edycja.</p>
                                
                                <p>Opracowanie ww. dokumentu umożliwi efektywne pozyskiwanie środków unijnych na
                                różne działania planowane do realizacji na terenie Gminy Wisznice w ramach
                                „Regionalnego Programu Operacyjnego Województwa Lubelskiego na lata 2014-2020”.
                                Posiadanie aktualnego Programu Rewitalizacji jest warunkiem niezbędnym do
                                aplikowania o środki zewnętrzne w ramach działania 13.4 „Rewitalizacja obszarów
                                wiejskich” RPO WL 2014-2020.</p>
                                <p>Zgodnie z definicją rewitalizacja to kompleksowy wieloletni program działań w sferze
                                społecznej oraz gospodarczej, przestrzenno-funkcjonalnej, technicznej i środowiskowej,
                                inicjowany w celu wyprowadzenia obszaru problemowego na terenie gminy ze stanu
                                kryzysowego poprzez nadanie mu nowej jakości funkcjonalnej i stworzenie warunków
                                do jego rozwoju.</p>
                                
                                <p>PW związku z powyższym, zapraszam wszystkich interesariuszy rewitalizacji w
                                tym: mieszkańców gminy, organizacje pozarządowe, przedsiębiorców oraz
                                osoby/instytucje zainteresowane rewitalizacją gminy na spotkanie pierwsze
                                informacyjno-konsultacyjne, które rozpocznie się 31.03.2017 r. (piątek) o godz.
                                15.00 w sali konferencyjnej Urzędu Gminy Wisznice.</p>
                                
                            </div>
                            <div class="half-col">
                                <p>Podczas spotkania przedstawione zostaną podstawowe zagadnienia związane
                                z rewitalizacją a także wstępnie wyznaczone na terenie gminy granice obszarów:
                                zdegradowanego i rewitalizacji. Ponadto przeprowadzona zostanie dyskusja mająca na
                                celu poznanie potrzeb i oczekiwań mieszkańców, a także określenie najważniejszych
                                problemów i potencjałów charakteryzujących wyznaczony obszar rewitalizacji.
                                Pragniemy aby wspólnie wypracowali Państwo główne założenia do przedmiotowego
                                dokumentu, które będą wynikały z Państwa potrzeb i oczekiwań.</p>
                                
                                <p>Udział społeczności lokalnej jest kluczowy dla opracowania Programu Rewitalizacji oraz
                                diagnozy społeczno-gospodarczej, w tym wyznaczenia obszarów rewitalizacji. Działania
                                planowane w ramach Programu mają służyć poprawie warunków życia i zwiększaniu
                                aktywności gospodarczej mieszkańców. Dlatego też zachęcamy do włączenia się do prac nad
                                dokumentem.</p>
                                <p>Jestem przekonany, że jako aktywni mieszkańcy, przedsiębiorcy i przedstawiciele
                                organizacji pozarządowych oraz instytucji publicznych, wniesiecie Państwo wiele uwag i
                                propozycji, które przyczynią się do zdiagnozowania kluczowych problemów
                                koniecznych do rozwiązania w procesie rewitalizacji oraz możliwych do wykorzystania
                                potencjałów obszaru rewitalizacji.</p>
                                <a href="assets/download/ZAPROSZENIE_Konsultacje.docx" class="file">ZAPROSZENIE_Konsultacje.docx</a>
                                <a href="assets/download/Plakat.jpg" class="cboxElement">
                                    <img src="assets/download/Plakat.jpg?ver=2" alt="Porozmawjamy o rewitalizacji Gminy Wisznice">
                                </a>
                                <p class="author"><strong>Wójt Gminy Wisznice<br/>Piotr Dragan</strong></p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="half-col">
                                <h3>Fiszka projektowa</h3>
                                <p><strong>Szanowni Państwo,</strong></p>
                                <p>Prace nad Lokalnym Programem Rewitalizacji Gminy Wisznice na lata 2017-2023 znajdują się w fazie
                                opracowywania konkretnych propozycji działań/przedsięwzięć (projektów), które będą podejmo-
                                wane w ramach tego Programu w najbliższych latach.</p>
                                <p>Prosimy zatem wszystkie osoby i instytucje oddane sprawie poprawy jakości życia w naszej gminie
                                o aktywne włączenie się w proces opracowywania Programu i opisanie swojego pomysłu, propozycji
                                projektu do Lokalnego Programu Rewitalizacji (zgodnie z zaproponowaną poniżej fiszką projektową),
                                bazując na wskazanym <strong>obszarze rewitalizacji obejmującym poniższe sołectwa:</strong></p>
                                <ul>
                                    <li>Curyn</li>
                                    <li>Rowiny</li>
                                    <li>część Wisznice</li>
                                </ul>
                            </div>
                            <div class="half-col">
                                <p>Obecnie wszczęta procedura naboru projektów jest elementem dalszych konsultacji Programu
                                Rewitalizacji, a także formalnego zebrania przedsięwzięć (projektów), które, po weryfikacji
                                przeprowadzonej przez zespół przedstawicieli wykonawcy dokumentu: Lubelskiej Fundacji Inicjatyw
                                Ekologicznej i Urzędu Gminy Wisznice, zostaną umieszczone w Lokalnym Programie Rewitalizacji
                                Gminy Wisznice na lata 2017-2023.</p>
                                
                                <a href="assets/download/FISZKA_PROJEKTOWA_Wisznice.docx" class="file">FISZKA_PROJEKTOWA_Wisznice.docx</a>
                                
                                <p class="author"><strong>Wójt Gminy Wisznice<br/>Piotr Dragan</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="aktualnosci right clearfix" id="cele">
                    <div class="narrow-col section-title">
                        <h2>Cele rewitalizacji</h2>
                        <img src="assets/images/cele.jpg" alt="Cele" />
                        <!--<h5>Pliki do pobrania</h5>
                        <a href="assets/download/FISZKA_PROJEKTOWA_Wisznice.docx" class="file right">lorem ipsum dolor sit amet lorem ipsum.docx</a>
                        <a href="assets/download/FISZKA_PROJEKTOWA_Wisznice.docx" class="file right">sprawozdanie.docx</a> -->
                        <!--Możliwe że będą pliki do pobrania-->
                    </div>
                    <div class="wide-col clearfix">
                        <div class="item">
                            <div class="half-col">
                                <h3>Podstawowe definicje związane z rewitalizacją</h3>
                                <p><strong>Rewitalizacja</strong> jest zbiorem kompleksowych działań, prowadzonych na rzecz lokalnej
                                społeczności, przestrzeni, gospodarki i środowiska, realizowana na obszarze zdegradowanym,
                                i wykorzystująca jego czasem niedostrzeżony potencjał.</p>
                                <p>Aby mówić o obszarze zdegradowanym należy rozpocząć od <strong>szczegółowej diagnozy</strong>,
                                obejmującej teren całej gminy. W pierwszej kolejności ustalany jest obszar, gdzie koncentrują się
                                <strong>negatywne zjawiska społeczne</strong> (w szczególności ubóstwo, przestępczość, bezrobocie, niski
                                poziom edukacji lub kapitału społecznego). Kolejnym krokiem jest sprawdzenie, czy na
                                wskazanym wyżej obszarze występują <strong>negatywne zjawiska gospodarcze, środowiskowe,
                                przestrzenno-funkcjonalno, techniczne.</strong> Jeżeli zdiagnozowane zostanie choć jedno z nich –
                                mamy do czynienia z <strong>obszarem zdegradowanym</strong>. Obszar taki, lub jego część, na którym
                                z uwagi na istotne znaczenie dla rozwoju lokalnego zamierza się prowadzić rewitalizację,
                                nazywamy <strong>obszarem rewitalizacji.</strong></p>
                                <p>Dla obszaru rewitalizacji przygotowywany jest <strong>program rewitalizacji</strong>, który musi być
                                odpowiedzią na zdiagnozowane problemy a przede wszystkim służyć wyprowadzeniu obszaru
                                zdegradowanego ze stanu kryzysowego. Rewitalizacja powinna być prowadzona przy udziale
                                <strong>interesariuszy</strong> rewitalizacji, którymi są: mieszkańcy obszaru rewitalizacji, właściciele,
                                użytkownicy wieczyści nieruchomości i podmioty zarządzające nieruchomościami znajdującymi
                                się na tym obszarze, w tym spółdzielnie mieszkaniowe, wspólnoty mieszkaniowe i towarzystwa
                                budownictwa społecznego. Ponad to: podmioty prowadzące lub zamierzające prowadzić
                                działalność gospodarczą lub społeczną na terenie gminy, w tym organizacje i pozarządowe
                                i grupy nieformalne, jednostki samorządu terytorialnego i ich jednostki organizacyjne, organy
                                władzy publicznej, podmioty realizujące na obszarze rewitalizacji uprawnienia Skarbu Państwa
                                a także wszyscy pozostali mieszkańcy gminy, którzy chcą uczestniczyć w tym procesie.</p>
                                <p>Rewitalizacja zakłada optymalne wykorzystanie specyficznych uwarunkowań danego obszaru
                                oraz wzmacnianie jego lokalnych potencjałów (w tym także kulturowych) i jest procesem
                                wieloletnim, prowadzonym przez interesariuszy tego procesu, w tym przede wszystkim we
                                współpracy z lokalną społecznością.</p>
                                <h4>Dla prowadzenia rewitalizacji wymagane są:</h4>
                                <ul>
                                    <li>uwzględnienie rewitalizacji jako istotnego elementu całościowej wizji rozwoju gminy,</li>
                                    <li>pełna diagnoza służąca wyznaczeniu obszaru rewitalizacji oraz analizie dotykających go
                                    problemów (diagnoza obejmuje kwestie społeczne oraz gospodarcze lub przestrzenno-
                                    funkcjonalne lub techniczne lub środowiskowe),</li>
                                    <li>ustalenie hierarchii potrzeb w zakresie działań rewitalizacyjnych,</li>
                                    <li>właściwy dobór narzędzi i interwencji do potrzeb i uwarunkowań danego obszaru,</li>
                                    <li>zsynchronizowanie działań w sferze społecznej, gospodarczej, przestrzenno-
                                    funkcjonalnej, technicznej, środowiskowej;</li>
                                    <li>koordynacja prowadzonych działań oraz monitorowanie i ewaluacja skuteczności
                                    rewitalizacji,</li>
                                    <li>realizacja zasady partnerstwa polegającej na włączeniu partnerów w procesy
                                    programowania i realizacji projektów rewitalizacyjnych w ramach programów
                                    operacyjnych oraz konsekwentnego, otwartego i trwałego dialogu z tymi podmiotami i
                                    grupami, których rezultaty rewitalizacji mają dotyczyć.</li>
                                </ul>
                                <p></p>
                            </div>
                            <div class="half-col">
                                <p><strong>Stan kryzysowy</strong> - stan spowodowany koncentracją negatywnych zjawisk społecznych
                                (w szczególności bezrobocia, ubóstwa, przestępczości, niskiego poziomu edukacji lub kapitału
                                społecznego, niewystarczającego poziomu uczestnictwa w życiu publicznym i kulturalnym),
                                współwystępujących z negatywnymi zjawiskami w co najmniej jednej z następujących sfer:</p>
                                <ul>
                                    <li>gospodarczej (w szczególności w zakresie niskiego stopnia przedsiębiorczości, słabej
                                    kondycji lokalnych przedsiębiorstw),</li>
                                    <li>środowiskowej (w szczególności w zakresie przekroczenia standardów jakości
                                    środowiska, obecności odpadów stwarzających zagrożenie dla życia, zdrowia, ludzi bądź
                                    stanu środowiska),</li>
                                    <li>przestrzenno-funkcjonalnej (w szczególności w zakresie niewystarczającego
                                    wyposażenia w infrastrukturę techniczną i społeczną, braku dostępu do podstawowych
                                    usług lub ich niskiej jakości, niedostosowania rozwiązań urbanistycznych do
                                    zmieniających się funkcji obszaru, niskiego poziomu obsługi komunikacyjnej, deficytu
                                    lub niskiej jakości terenów publicznych),</li>
                                    <li>technicznej (w szczególności w zakresie degradacji stanu technicznego obiektów
                                    budowlanych, w tym o przeznaczeniu mieszkaniowym, oraz braku funkcjonowania
                                    rozwiązań technicznych umożliwiających efektywne korzystanie z obiektów
                                    budowlanych, w szczególności w zakresie energooszczędności i ochrony środowiska).
                                    Skalę negatywnych zjawisk odzwierciedlają mierniki rozwoju opisujące powyższe sfery,
                                    które wskazują na niski poziom rozwoju lub dokumentują silną dynamikę spadku
                                    poziomu rozwoju, w odniesieniu do wartości dla całej gminy.</li>
                                </ul>
                                <p><strong>Obszar zdegradowany</strong> - obszar, na którym zidentyfikowano stan kryzysowy. Dotyczy to
                                najczęściej obszarów miejskich, ale także wiejskich. Obszar zdegradowany może być podzielony
                                na podobszary, w tym podobszary nieposiadające ze sobą wspólnych granic pod warunkiem
                                stwierdzenia sytuacji kryzysowej na każdym z podobszarów.</p>
                                <p>Obszar rewitalizacji - obszar obejmujący całość lub część obszaru zdegradowanego,
                                cechującego się szczególną koncentracją negatywnych zjawisk, , z uwagi na istotne znaczenie dla
                                rozwoju lokalnego, zamierza się prowadzić rewitalizację. Obszar rewitalizacji może być
                                podzielony na podobszary, w tym podobszary nieposiadające ze sobą wspólnych granic, <strong>lecz nie
                                może obejmować terenów większych niż 20% powierzchni gminy oraz zamieszkałych
                                przez więcej niż 30% mieszkańców gminy.</strong> W skład obszaru rewitalizacji mogą wejść obszary
                                występowania problemów przestrzennych, takich jak tereny poprzemysłowe (w tym poportowe
                                i powydobywcze), powojskowe lub pokolejowe, wyłącznie w przypadku, gdy przewidziane dla
                                nich działania są ściśle powiązane z celami rewitalizacji dla danego obszaru rewitalizacji.</p>
                                <p><strong>Program rewitalizacji</strong> - inicjowany, opracowany i uchwalony przez radę gminy wieloletni
                                program działań w sferze społecznej oraz gospodarczej lub przestrzenno-funkcjonalnej lub
                                technicznej lub środowiskowej, zmierzający do wyprowadzenia obszarów rewitalizacji ze stanu
                                kryzysowego oraz stworzenia warunków do ich zrównoważonego rozwoju, stanowiący
                                narzędzie planowania, koordynowania i integrowania różnorodnych aktywności w ramach
                                rewitalizacji.</p>
                                <p class="author"><strong>Wójt Gminy Wisznice<br/>Piotr Dragan</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kontakt clearfix" id="kontakt">
                    <div class="narrow-col section-title">
                        <h2>Kontakt</h2>
                    </div>
                    <div class="wide-col clearfix">
                        <div class="half-col">
                            <h2>Urząd Gminy Wisznice</h2>
                            <p>ul. Rynek 35<br />
                                21-580 Wisznice</p>
                            <p>tel. +48 (083) 378-21-02<br />
                                fax: +48 (083) 378-20-39</p>
                            <p>e-mail: gmina@wisznice.pl<br />
                                www.wisznice.pl</p>
                        </div>
                        <div class="half-col">
                            <p>Wykonawca programu rewitalizacji:<br />
                            Lubelska Fundacja Inicjatyw Ekologicznych</p>
                            <p>Arkadiusz Pisarski<br />
                            tel. +48 661 055 199</p>
                            <p>e-mail: arkadiusz.pisarski@lfie.pl</p>
                        </div>
                    </div>
                </div>
            </div>
            <img src="assets/images/ue.jpg" alt="Fundusze europejskie" class="ue-logos" />
        </div>
        <script src="assets/js/jquery-1.12.3.min.js"></script>
        <script src="assets/js/jquery.colorbox.js"></script>
        <script src="assets/js/scripts.js?ver=2"></script>
    </body>
</html>
